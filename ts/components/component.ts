class Component {
    protected containerElement: HTMLElement;
    
    constructor(element: HTMLElement) {
        this.containerElement = element;
    }
}

export default Component;